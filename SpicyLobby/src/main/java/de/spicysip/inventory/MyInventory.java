package de.spicysip.inventory;

import de.spicysip.core.api.spigot.inventory.IInventory;
import de.spicysip.core.api.spigot.inventory.item.IItem;
import de.spicysip.core.api.spigot.inventory.item.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 15:39
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class MyInventory implements IInventory {

    @Override
    public String getInventoryIdentifier() {
        return "my-inventory";
    }

    @Override
    public String getInventoryTitle() {
        return "The title!";
    }

    @Override
    public int getInventorySize() {
        return 9;
    }

    @Override
    public boolean fullUnclickable() {
        return true;
    }

    @Override
    public Map<Integer, IItem> getInventoryContents() {
        Map<Integer, IItem> contents = new HashMap<>();

        contents.put(1, new ItemBuilder(Material.APPLE).setName("Apple").build());

        contents.put(2, new ItemBuilder(Material.APPLE).setName("Apple").build(event -> {
            event.getWhoClicked().sendMessage("gellpe");
        }));

        return contents;
    }
}
