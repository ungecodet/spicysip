package de.spicysip.game;

import de.spicysip.core.api.spigot.game.AbstractGame;
import de.spicysip.game.states.IngameState;

import java.util.Arrays;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 15:36
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class BedWars extends AbstractGame {

    @Override
    public String getGameIdentifier() {
        return "bedwars";
    }

    @Override
    public void loadGameStates() {
        gameStates.addAll(Arrays.asList(
                new IngameState()
        ));
    }
}
