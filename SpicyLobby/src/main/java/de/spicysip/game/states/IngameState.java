package de.spicysip.game.states;

import de.spicysip.core.api.spigot.game.state.IGameState;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 15:38
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class IngameState implements IGameState {

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }
}
