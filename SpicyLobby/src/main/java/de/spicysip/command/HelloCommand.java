package de.spicysip.command;

import de.spicysip.board.MyScoreboard;
import de.spicysip.core.api.command.AbstractSpigotCommand;
import de.spicysip.core.api.command.CommandInfo;
import de.spicysip.core.api.spigot.board.IScoreboardManager;
import de.spicysip.core.api.spigot.inventory.IInventoryManager;
import de.spicysip.core.api.spigot.inventory.InventoryByIdFilter;
import de.spicysip.inventory.MyInventory;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

@CommandInfo(
        commandVersion = CommandInfo.CommandVersion.SPIGOT,
        name = "hello"
)
public class HelloCommand extends AbstractSpigotCommand {

    private final IScoreboardManager scoreboardManager;

    public HelloCommand(JavaPlugin plugin, IScoreboardManager scoreboardManager) {
        super(plugin);

        this.scoreboardManager = scoreboardManager;
    }

    @Override
    public boolean onSpicyCommand(CommandSender executor, String[] args) {
        executor.sendMessage("hello!");

        if (executor instanceof Player)
            scoreboardManager.sendScoreboard(((Player) executor), new MyScoreboard(scoreboardManager));

        return true;
    }

    @Override
    public void sendHelp(CommandSender executor) {
        executor.sendMessage("Help!");
    }

}
