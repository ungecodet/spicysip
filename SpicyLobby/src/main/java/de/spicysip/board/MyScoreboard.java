package de.spicysip.board;

import de.spicysip.board.team.MyScoreboardTeam;
import de.spicysip.core.api.spigot.board.IRow;
import de.spicysip.core.api.spigot.board.IScoreboard;
import de.spicysip.core.api.spigot.board.IScoreboardManager;
import de.spicysip.core.api.spigot.board.team.IScoreboardTeam;
import de.spicysip.core.api.spigot.board.team.TeamIdentifierFilter;
import de.spicysip.core.api.utils.IManager;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.HashMap;
import java.util.Map;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 15:32
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class MyScoreboard implements IScoreboard {

    private final IManager<IScoreboardTeam> teamManager;

    public MyScoreboard(IScoreboardManager scoreboardManager) {
        scoreboardManager.add(this);

        this.teamManager = scoreboardManager.getScoreboadTeamManager();
        this.teamManager.add(new MyScoreboardTeam(player));
    }

    @Override
    public String getScoreboardIdentifier() {
        return "my-scoreboard";
    }

    @Override
    public DisplaySlot getScoreboardType() {
        return DisplaySlot.SIDEBAR;
    }

    @Override
    public String getScoreboardTitle() {
        return "Mein Titel!";
    }

    @Override
    public Map<Integer, IRow> getRows() {
        Map<Integer, IRow> rows = new HashMap<>();

        rows.put(1, () -> (
                "Hallo"
        ));

        rows.put(2, teamManager.getByFilter(new TeamIdentifierFilter("my-scoreboard-team")).findFirst().get());

        return rows;
    }
}
