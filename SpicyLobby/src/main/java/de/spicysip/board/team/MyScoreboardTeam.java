package de.spicysip.board.team;

import de.spicysip.core.api.spigot.board.team.IScoreboardTeam;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 15:34
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class MyScoreboardTeam implements IScoreboardTeam {

    private Player player;

    @Override
    public String getValue() {
        return String.valueOf(player.getHealth());
    }

    @Override
    public String getTeamIdentifier() {
        return "my-scoreboard-team";
    }

    @Override
    public String getPrefix() {
        return "BEGIN:";
    }

    @Override
    public String getSuffix() {
        return ":END";
    }

    @Override
    public ChatColor getEntry() {
        return ChatColor.BLUE;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
