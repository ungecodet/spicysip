package de.spicysip.core;

import de.spicysip.board.MyScoreboard;
import de.spicysip.command.HelloCommand;
import de.spicysip.core.api.AbstractSpicySpigot;
import de.spicysip.core.api.command.AbstractCommand;
import de.spicysip.core.api.command.ICommandManager;
import de.spicysip.core.api.spigot.board.IScoreboardManager;
import de.spicysip.core.api.utils.IManager;
import org.bukkit.command.CommandSender;

/**
 * @author SoftwareBuilds
 * @since 05.08.2023 22:32
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public final class SpicySpigot extends AbstractSpicySpigot {

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEnable() {
        super.onEnable();

        ICommandManager commandManager = getInstance().getCommandManager();
        IScoreboardManager scoreboardManager = getInstance().getScoreboardManager();

        scoreboardManager.add(new MyScoreboard(scoreboardManager));


        HelloCommand helloCommand = new HelloCommand(this, scoreboardManager);
        commandManager.add(helloCommand);
        commandManager.registerAllCommands();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDisable() {

    }
}
