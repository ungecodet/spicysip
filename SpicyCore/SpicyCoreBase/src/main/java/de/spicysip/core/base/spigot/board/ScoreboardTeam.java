package de.spicysip.core.base.spigot.board;

import de.spicysip.core.api.spigot.board.team.IScoreboardTeam;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author SoftwareBuilds
 * @since 10.08.2023 21:58
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class ScoreboardTeam implements IScoreboardTeam {

    private Scoreboard scoreboard;

    private String value;

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public String getTeamIdentifier() {
        return null;
    }

    @Override
    public String getPrefix() {
        return null;
    }

    @Override
    public String getSuffix() {
        return null;
    }

    @Override
    public ChatColor getEntry() {
        return null;
    }

    private Team buildTeam() {
        Team team = scoreboard.getTeam(getTeamIdentifier());

        team.setPrefix(getPrefix() + getValue());
        team.setSuffix(getSuffix());

        team.addEntry(getEntry().toString());

        return team;
    }
}
