package de.spicysip.core.base.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.spicysip.core.api.config.IConfig;
import de.spicysip.core.api.config.IConfigManager;

import java.io.*;
import java.util.concurrent.CompletableFuture;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 01:03
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class ConfigManager<T> implements IConfigManager<T> {

    /**
     * The gson reader and writer for json files.
     */
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();

    /**
     * {@inheritDoc}
     */
    @Override
    public IConfig<T> loadConfiguration(Class<T> tClass, File file) {
        try (FileReader reader = new FileReader(file)) {
            return (IConfig<T>) GSON.fromJson(reader, tClass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletableFuture<IConfig<T>> loadConfigurationAsync(Class<T> tClass, File file) {
        return CompletableFuture.supplyAsync(() -> loadConfiguration(tClass, file));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveConfiguration(Class<T> tClass, IConfig<T> config, File file) {
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(GSON.toJson(config));
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
