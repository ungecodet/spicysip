package de.spicysip.core.base.spigot.board;

import de.spicysip.core.api.spigot.board.IScoreboard;
import de.spicysip.core.api.spigot.board.IScoreboardManager;
import de.spicysip.core.api.spigot.board.team.IScoreboardTeam;
import de.spicysip.core.api.utils.IManager;
import de.spicysip.core.base.utils.Manager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 14:43
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class ScoreboardManager extends Manager<IScoreboard> implements IScoreboardManager {

    /**
     * Manager for scoreboard teams.
     */
    private final IManager<IScoreboardTeam> scoreboardTeam = new Manager<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendScoreboard(Player player, IScoreboard scoreboard) {
        Scoreboard spigotScoreboard = buildSpigotScoreboard(player, scoreboard);
        player.setScoreboard(spigotScoreboard);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clearScoreboard(Player player) {
        player.setScoreboard(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IManager<IScoreboardTeam> getScoreboadTeamManager() {
        return scoreboardTeam;
    }

    /**
     * Builds the scoreboard for minecraft.
     *
     * @param scoreboard The simple scoreboard.
     *
     * @return The builded minecraft scoreboard.
     */
    private static Scoreboard buildSpigotScoreboard(Player player, IScoreboard scoreboard) {
        Scoreboard spigotScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = spigotScoreboard.registerNewObjective("xxx", "yyy");

        objective.setDisplaySlot(scoreboard.getScoreboardType());
        objective.setDisplayName(scoreboard.getScoreboardTitle());

        scoreboard.getRows().forEach((position, row) -> {
            objective.getScore(row.getValue()).setScore(position);
        });

        return spigotScoreboard;
    }
}
