package de.spicysip.core.base.log;

import de.spicysip.core.api.log.ILogger;
import de.spicysip.core.api.log.LogType;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 01:09
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class Logger implements ILogger {

    /**
     * The logger channel
     */
    private final String channel;

    /**
     * {@inheritDoc}
     */
    public Logger(String channel) {
        this.channel = channel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void log(LogType type, String message) {
        System.out.println(String.format("[%s][%s] > %s", channel, type.name(), message));
    }
}
