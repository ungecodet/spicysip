package de.spicysip.core.base.plugin;

import de.spicysip.core.api.command.ICommandManager;
import de.spicysip.core.api.language.ILanguage;
import de.spicysip.core.api.log.ILogger;
import de.spicysip.core.api.plugin.IPluginCore;
import de.spicysip.core.api.config.IConfigManager;
import de.spicysip.core.api.database.IDatabaseManager;
import de.spicysip.core.api.spigot.board.IScoreboardManager;
import de.spicysip.core.api.spigot.game.IGame;
import de.spicysip.core.api.spigot.inventory.IInventoryManager;
import de.spicysip.core.api.utils.IManager;
import de.spicysip.core.base.command.CommandManager;
import de.spicysip.core.base.config.ConfigManager;
import de.spicysip.core.base.log.Logger;
import de.spicysip.core.base.spigot.board.ScoreboardManager;
import de.spicysip.core.base.spigot.inventory.InventoryManager;
import de.spicysip.core.base.utils.Manager;

/**
 * @author SoftwareBuilds
 * @since 07.08.2023 20:17
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class PluginCore<P> implements IPluginCore {

    /**
     * The command manager.
     */
    private final ICommandManager commandManager;

    /**
     * The database manager.
     */
    private IDatabaseManager databaseManager;

    /**
     * The scoreboard manager.
     */
    private final IScoreboardManager scoreboardManager;

    /**
     * The language manager.
     */
    private final IManager<ILanguage> languageManager;

    /**
     * The game manager.
     */
    private final IManager<IGame> gameManager;

    /**
     * The inventory manager.
     */
    private final InventoryManager inventoryManager;

    /**
     * Construct the plugin core.
     *
     * @param plugin The instance of sub plugin.
     */
    public PluginCore(P plugin) {
        this.commandManager = new CommandManager<>();
        this.scoreboardManager = new ScoreboardManager();
        this.languageManager = new Manager<>();
        this.gameManager = new Manager<>();
        this.inventoryManager = new InventoryManager();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ICommandManager getCommandManager() {
        return commandManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IManager<IGame> getGameManager() {
        return gameManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IManager<ILanguage> getLanguageManager() {
        return languageManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ILogger createLogger(String channel) {
        return new Logger(channel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IInventoryManager getInventoryManager() {
        return inventoryManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> IConfigManager<T> getConfigManager() {
        return new ConfigManager<>();
    }

}
