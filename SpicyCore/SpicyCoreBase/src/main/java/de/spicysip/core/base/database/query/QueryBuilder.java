package de.spicysip.core.base.database.query;

import de.spicysip.core.api.database.IModel;
import de.spicysip.core.api.database.meta.Column;
import de.spicysip.core.api.database.meta.Relation;
import de.spicysip.core.api.database.query.CommandType;
import de.spicysip.core.api.database.query.IQuery;
import de.spicysip.core.api.database.query.IQueryBuilder;
import de.spicysip.core.base.database.meta.WhereCondition;

/**
 * @author SoftwareBuilds
 * @since 10.08.2023 11:39
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public class QueryBuilder<M extends IModel> implements IQueryBuilder<M> {

    /**
     * The model query.
     */
    private Query<M> query;

    /**
     * {@inheritDoc}
     */
    @Override
    public IQueryBuilder<M> setCommandType(CommandType type) {
        query.setCommandType(type);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <V> IQueryBuilder<M> where(Column column, Operand operand, V needle) {
        query.setWhereCondition(new WhereCondition<>(column, operand, needle));
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <V> IQueryBuilder<M> orWhere(Column column, Operand operand, V needle) {
        query.getAdaptiveWheres().add(new WhereCondition<>(column, operand, needle));
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <V> IQueryBuilder<M> andWhere(Column column, Operand operand, V needle) {
        query.getAdaptiveWheres().add(new WhereCondition<>(column, operand, needle));
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <O extends IModel> IQueryBuilder<M> getRelatedModel(IQueryBuilder<O> queryBuilder, Relation relation) {
        /*IQuery<M> sourceModelQuery = build();
        sourceModelQuery.execute();

        IModelInformation modelInformation = M.getModelInformation(sourceModelQuery.getResult().getClass());

        queryBuilder.where(relation.foreignColumn(), Operand.EQUAL, modelInformation.getFieldFromRelation().)
*/
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IQuery<M> build() {
        return query;
    }
}
