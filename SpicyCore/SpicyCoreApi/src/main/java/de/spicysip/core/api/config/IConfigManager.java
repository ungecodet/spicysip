package de.spicysip.core.api.config;

import java.io.File;
import java.util.concurrent.CompletableFuture;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 00:44
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public interface IConfigManager<T> {

    /**
     * Loads a configuration specific to a type.
     *
     * @param tClass The type class.
     * @param file   The file.
     *
     * @return The key value map with value as type values.
     */
    IConfig<T> loadConfiguration(Class<T> tClass, File file);

    /**
     * Loads a configuration specific to a type async.
     *
     * @see #loadConfiguration(Class, File)
     *
     * @param tClass The type class.
     * @param file   The file.
     *
     * @return The key value map with value as type values.
     */
    default CompletableFuture<IConfig<T>> loadConfigurationAsync(Class<T> tClass, File file) {
        return CompletableFuture.supplyAsync(() -> loadConfiguration(tClass, file));
    }

    /**
     * Saves the type specific content to a file.
     *
     * @param tClass The type class.
     * @param config The content which would be saved.
     * @param file   The file.
     *
     * @return The state if saved.
     */
    boolean saveConfiguration(Class<T> tClass, IConfig<T> config, File file);

    /**
     * Save a configuration specific to a type async.
     *
     * @see #saveConfiguration(Class, IConfig, File)
     *
     * @param tClass The type class.
     * @param file   The file.
     *
     * @return The state if saved.
     */
    default CompletableFuture<Boolean> saveConfigurationAsync(Class<T> tClass, IConfig<T> config, File file) {
        return CompletableFuture.supplyAsync(() -> saveConfiguration(tClass, config, file));
    }

}
