package de.spicysip.core.api.spigot.board.type;

public enum ScoreboardType {

    BOARD, TABLIST

}
