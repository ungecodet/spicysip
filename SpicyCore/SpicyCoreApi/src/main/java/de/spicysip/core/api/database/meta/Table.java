package de.spicysip.core.api.database.meta;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 15:33
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public @interface Table {

    /**
     * The table name.
     *
     * @return Table name.
     */
    String name();

}
