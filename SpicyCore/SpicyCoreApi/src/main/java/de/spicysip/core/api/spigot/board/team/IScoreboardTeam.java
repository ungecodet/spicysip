package de.spicysip.core.api.spigot.board.team;

import de.spicysip.core.api.spigot.board.IRow;
import org.bukkit.ChatColor;

/**
 * @author SoftwareBuilds
 * @since 05.08.2023 23:05
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public interface IScoreboardTeam extends IRow {

    /**
     * Get the team identifier.
     *
     * @return The identifier.
     */
    String getTeamIdentifier();

    /**
     * Get the prefix in row.
     *
     * @return The prefix.
     */
    String getPrefix();

    /**
     * Get the suffix in row.
     *
     * @return The suffix.
     */
    String getSuffix();

    /**
     * The update entry.
     *
     * @return The entry cc.
     */
    ChatColor getEntry();
}
