package de.spicysip.core.api.database.meta.type;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 15:34
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public enum DataType {

    TEXT, VARCHAR, INT

}
