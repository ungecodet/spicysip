package de.spicysip.core.api.database.query;

/**
 * @author SoftwareBuilds
 * @since 06.08.2023 15:27
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public enum CommandType {

    SELECT, DELETE, INSERT, UPDATE

}
