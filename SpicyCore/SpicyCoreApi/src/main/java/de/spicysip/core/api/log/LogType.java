package de.spicysip.core.api.log;

/**
 * @author SoftwareBuilds
 * @since 05.08.2023 23:10
 * Copyright © 2023 | SoftwareBuilds | All rights reserved.
 */
public enum LogType {

    INFO, WARNING, ERROR

}
